Listing 2.6. Kod HTML z połowy lat 90. ubiegłego wieku, niezawierający podziału na warstwy: struktura, wygląd i zachowanie
<!DOCTYPE...>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
  <head>
    <title>Ćwiczenie 2.4</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
<body>

<ul>
    <li onclick="alert('Witaj')"><font face="Georgia" color="#970da6">lorem</font></li>
    <li onclick="alert('Witaj')"><font face="Georgia" color="#970da6">ipsum</font></li>
    <li onclick="alert('Witaj')"><font face="Georgia" color="#970da6">dolor</font></li>
    <li onclick="alert('Witaj')"><font face="Georgia" color="#970da6">sit</font></li>
    <li onclick="alert('Witaj')"><font face="Georgia" color="#970da6">amet</font></li>
</ul>

</body>
</html>