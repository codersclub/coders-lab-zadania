Listing 17.3. Równoważność instrukcji andSelf() i add(this) wewnątrz funkcji obsługi zdarzenia
$(function(){
    $('li').mouseover(function(){
        $(this).prevAll().andSelf().css('background', 'red');
    }).mouseout(function(){
        $(this).prevAll().andSelf().css('background', 'white');
    });
});