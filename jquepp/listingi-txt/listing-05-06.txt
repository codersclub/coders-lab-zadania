Listing 5.6. Plik index.html - rozwiązanie ćwiczenia 5.5
//kod jQuery
$(function(){
    $('a').mouseover(function(){
        $('h2#wskaznik').text($(this).text());
    }).mouseout(function(){
        $('h2#wskaznik').text('');
    });
});

//kod XHTML
<ul>
    <li><a href="#">lorem</a></li>
    <li><a href="#">ipsum</a></li>
    <li><a href="#">dolor</a></li>
</ul>
<h2 id="wskaznik"></h2>