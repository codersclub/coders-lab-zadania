Listing 7.5. Rozwiązanie ćwiczenia 7.4
//kod jQuery
$(function(){
    $('div').after('<p>A</p>');
    $('div').before('<p>B</p>');
    $('<p>C</p>').insertAfter('div');
    $('<p>D</p>').insertBefore('div');
});