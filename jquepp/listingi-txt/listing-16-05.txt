Listing 16.5. Testowanie działania funkcji .replaceAll()
//kod jQuery
$(function(){
    var t = $('strong').replaceAll('em');
    $('#info').text(t.text());
});

//kod XHTML
<div>
<strong>one</strong>
<strong>two</strong>
</div>

<p>
<em>Lorem</em> ipsum <em>dolor</em> sit <em>amet</em>...
</p>

<div>
<strong>three</strong>
<strong>four</strong>
</div>

<div id="info"></div>