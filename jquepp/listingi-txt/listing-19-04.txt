Listing 19.4. Użycie wtyczki z ćwiczenia 19.1 do modyfikacji akapitów i elementów li
<!DOCTYPE...>
<html>
  <head>
    <title>Ćwiczenie 19.2</title>
    <script type="text/javascript" src="jquery-1.4.min.js"></script>
    <script type="text/javascript" src="jquery.powitanie.js"></script>
    <script type="text/javascript">
    $(function(){
        $('li').add('p').powitanie();
    });
    </script>
  </head>
<body>
    <ul>
        <li>raz</li>
        <li>dwa</li>
        <li>trzy</li>
        <li>cztery</li>
        <li>pięć</li>
    </ul>

    <p>lorem ipsum...</p>

    <p>dolor sit amet...</p>
</body>
</html>