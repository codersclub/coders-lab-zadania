Listing 11.4. Rozwiązanie ćwiczenia 11.2
<script type="text/javascript">
trwa = 0;
function wylacz_hiperlacza()
{
    trwa = 1;
    $('a').
        css('text-decoration', 'none').
        hover(
            function(){
                $(this).css('text-decoration', 'none');
            },
            function(){
                $(this).css('text-decoration', 'none');
            }
        );
}

function wlacz_hiperlacza()
{
    trwa = 0;
    $('a').
        css('text-decoration', 'none').
        hover(
            function(){
                $(this).css('text-decoration', 'underline');
            },
            function(){
                $(this).css('text-decoration', 'none');
            }
        );
}

$(function(){
    wylacz_hiperlacza();
    $('#tresc').hide();
    $('#tresc').load('fragment-ulica-dominikanska.html');
    $('#tresc').fadeIn(2000, function(){
        wlacz_hiperlacza();
    });

    $('a').click(function(){
        if (trwa == 0) {
            wylacz_hiperlacza();
            var url = $(this).attr('href');
            $('#tresc').fadeOut(2000, function(){
                $('#tresc').load(url);
                $('#tresc').fadeIn(2000, function(){
                    wlacz_hiperlacza();
                });
            });
        }
        return false;
    });
});
</script>