Listing 7.11. Przepisywanie treści formularza do elementu div
//kod jQuery
$(document).ready(function(){
    $('input').keypress(function(e){
        if (
            e.which == 32 ||
            (65 <= e.which && e.which <= 65 + 25) ||
            (97 <= e.which && e.which <= 97 + 25)
        ) {
            var c = String.fromCharCode(e.which);
            $('<span>' + c + '</span>').appendTo('div');
        } else if (e.which == 8) {
            $('span').remove();
            $(this).attr('value', '');
        }
    });
});


//kod XHTML
<body>
    <form action="#" method="post">
        <p>
            <input type="text" name="imie" value="" />
        </p>
    </form>
    <div></div>
</body>