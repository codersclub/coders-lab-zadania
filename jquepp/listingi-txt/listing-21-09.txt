Listing 21.9. Wtyczka z pliku jquery.kolekcja.js
(function($) {
    $.fn.kolekcja = function(parametr) {
        var opcje = {
            src: 'mini/',
            dest: 'popup/',
            pliki: []
        };
        if (parametr) {
            $.extend(opcje, parametr);
        }
        if (opcje.pliki.length > 0) {
            var el = this.first();
            $('<div id="popup"><img src="null.jpg" alt="" /></div>').prependTo('body');
            $.each(opcje.pliki, function(index, value){
                var obraz =
                    $('<img src="' + opcje.src + opcje.pliki[index] + '" alt="" />')
                        .mouseover(function(){
                            var maly = $(this).attr('src');
                            var duzy = maly.replace(opcje.src, opcje.dest);
                            $('#popup img').attr('src', duzy);
                            $('#popup').show();
                        })
                        .mouseout(function(){
                            $('#popup').hide();
                        })
                        .mousemove(function(e){
                            $('#popup').css('left', e.pageX + 10);
                            $('#popup').css('top', e.pageY + 10);
                        })
                        .wrap('<div class="minifotka"></div>')
                        .parent()
                        .appendTo(el);
            });
        }
        return this;
    };
})(jQuery);