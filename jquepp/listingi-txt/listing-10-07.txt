Listing 10.7. Plik index.html zawiera menu oraz element div przeznaczony na treść wierszy
<body>
<h1>Ignacy Krasicki</h1>
<h2>Bajki i przypowieści. Wybór</h2>
<p>
    <a href="kulawy-i-slepy.html">Kulawy i ślepy</a>
    <a href="orzel-i-jastrzab.html">Orzeł i jastrząb</a>
    <a href="szczur-i-kot.html">Szczur i kot</a>
</p>
<div>Wierszyk...</div>
</body>