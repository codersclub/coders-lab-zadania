Listing 9.8. Tabela XHTML o wielu wierszach
<table>
    <tr>
        <th>Infinitive</th>
        <th>Past Simple</th>
        <th>Past Participle</th>
        <th>Tłumaczenie</th>
    </tr>
    <tr>
        <td>awake</td>
        <td>awoke</td>
        <td>awoken</td>
        <td>budzić się</td>
    </tr>
    <tr>
        <td>bear</td>
        <td>bore</td>
        <td>borne</td>
        <td>rodzić, nosić</td>
    </tr>
    ...
<table>