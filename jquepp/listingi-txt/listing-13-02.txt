Listing 13.2. Tworzenie fragmentu drzewa DOM za pomocą funkcji $()
<!DOCTYPE...>
<html>
  <head>
    <title>Ćwiczenie 13.2</title>
    <script type="text/javascript" src="jquery-1.4.min.js"></script>
    <script type="text/javascript">
    $(function(){
        $('<div class="element"><h2>Lorem</h2><p>Ipsum<em> dolor</em></p></div>').appendTo('body');
    });
    </script>
  </head>
<body>
</body>
</html>