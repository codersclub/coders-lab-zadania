Listing 13.5. Wywołanie metody $(selektor, kontekst), w którym w roli kontekstu występuje zmienna this
//kod jQuery
$(function(){
    $('div').click(function(){
        $('p', this).toggle();
    });
});

//kod XHTML
<body>
<div>
    <h2>Lorem</h2>
    <p>Lorem ipsum...</p>
</div>
<div>
    <h2>Dolor</h2>
    <p>Dolor sit amet...</p>
</div>
</body>