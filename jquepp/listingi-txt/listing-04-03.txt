Listing 4.3. Rozwiązanie ćwiczenia 4.2
$(function(){
    $('.kolory div').mouseover(function(){
        $('p').css('background', $(this).css('background-color'));
    }).mouseout(function(){
        $('p').css('background', 'white');
    });
});