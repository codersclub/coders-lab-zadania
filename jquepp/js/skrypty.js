$(function(){
    $('.przyklad').fotkapopup('100x75/', '400x300/');

    $('.ksiazka_element:gt(1)').append(' <a href="#top" title="Przewiń na początek strony">Przewiń na początek strony</a>');

    $('a[href="#top"]').click(function(event){
        event.preventDefault();
        $.scrollTo($('body'), 2000);
    });


    var okladka = $('.metryka .stala img').attr('src');
    if (okladka) {
    
	    var okladka_back = okladka.replace('-duza.jpg', '-back.jpg');    
	    var preloadImage = $('<img />').attr('src', okladka_back);        
	
	    $('.metryka .stala img').everyTime(3000, function(){
	        var plk = $(this).attr('src');
	        var re = /-duza\.jpg$/;
	        wynik = plk.match(re);
	        if (wynik) {
	            $(this).css({opacity: 1}).animate({opacity: 0}, 400, function(){
	                var duza = $(this).attr('src');
	                var back = duza.replace('-duza.jpg', '-back.jpg');
	                $(this).attr('src', back);
	                $(this).css({opacity: 0}).animate({opacity: 1.0}, 400);
	            });
	        } else {
	            $(this).css({opacity: 1}).animate({opacity: 0}, 400, function(){
	                var back = $(this).attr('src');
	                var duza = back.replace('-back.jpg', '-duza.jpg');
	                $(this).attr('src', duza);
	                $(this).css({opacity: 0}).animate({opacity: 1.0}, 400);
	            });
	        }
	    });
	
    
    }

});