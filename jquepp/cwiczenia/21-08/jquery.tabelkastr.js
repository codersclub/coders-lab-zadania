(function($) {

    $.fn.tabelkastr = function(ile_na_stronie) {
    
        function ukryjHiperlacze(e)
        {
            e.children().eq(0).hide();
            e.children().eq(1).show();
        }

        function pokazHiperlacze(e)
        {
            e.children().eq(0).show();
            e.children().eq(1).hide();
        }

        if (!ile_na_stronie) {
            ile_na_stronie = 10;
        }

        /*
        tworzymy strukturę XHTML
<div class="tabelkastr">
    <p>
        <span class="prev"><a href="#">prev</a><span>prev</span></span>
        <span class="next"><a href="#">next</a><span>next</span></span>
    </p>
    <div>
        <table>...</table>
    </div>
</div>
        */
        
        var element = this.wrap('<div class="tabelkastr><div></div></div>').parent().parent();
        element.prepend('<p><span class="prev"><a href="#">prev</a><span>prev</span></span> <span class="next"><a href="#">next</a><span>next</span></span></p>');
        
        ukryjHiperlacze(element.find('.prev'));
        pokazHiperlacze(element.find('.next'));

        var tablica_danych = [];
        $('tr:first', element).nextAll().each(function(){
            tablica_danych.push($(this).children(':eq(0)').text());
        });

        $('div', element).html('');
        while (tablica_danych.length > 0) {
            var tabelka = $('<table><tr><th class="k2">Imię</th></tr></table>');
            for (i = 0; (i < ile_na_stronie) && (tablica_danych.length > 0); i = i + 1) {
                tabelka.append('<tr><td>' + tablica_danych.shift() + '</td></tr>');
            }
            $('div', element).append(tabelka);
        }

        $('table', element).first().nextAll().hide();
        
        $('.next a', element).click(function(){
            pokazHiperlacze(element.find('.prev'));
            $('div', element).children().filter(':visible').hide().next().show();
            if ($('div', element).children().filter(':visible').is(':last-child')) {
                ukryjHiperlacze(element.find('.next'));
            };
        });

        $('.prev a', element).click(function(){
            pokazHiperlacze(element.find('.next'));
            $('div', element).children().filter(':visible').hide().prev().show();
            if ($('div', element).children().filter(':visible').is(':first-child')) {
                ukryjHiperlacze(element.find('.prev'));
            };
        });

        return this;
    };
    
})(jQuery);