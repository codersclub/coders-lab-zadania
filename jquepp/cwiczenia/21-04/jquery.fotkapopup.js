(function($) {
    $.fn.fotkapopup = function(src, dest) {
        
        if (!(src && dest)) {
            src =  'mini/';
            dest = 'popup/';
        }
        
        $('<div id="popup"><img src="null.jpg" alt="" /></div>').prependTo('body');
        
        this.each(function() {
            $(this).find('img').mouseover(function(){
                var maly = $(this).attr('src');
                var duzy = maly.replace(src, dest);
                $('#popup img').attr('src', duzy);
                $('#popup').show();
            }).mouseout(function(){
                $('#popup').hide();
            }).mousemove(function(e){
                $('#popup').css('left', e.pageX + 10);
                $('#popup').css('top', e.pageY + 10);
            });
        });
        return this;
    };
})(jQuery);