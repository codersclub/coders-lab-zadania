var imiona = new Array(
    'Jan',
    'Jarek',
    'Roman',
    'Witold',
    'Anna',
    'Joanna',
    'Izabella'
);

imiona.sort();

var kodHtml = '<table class="tabelka"><tr><th>Imię</th></tr>';
for(imie in imiona){
    kodHtml = kodHtml + '<tr><td>' + imiona[imie] + '</td></tr>';
}
kodHtml = kodHtml + '</table>';

$(function(){
    $('#imiona').html(kodHtml);
});