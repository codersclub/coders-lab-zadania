(function($) {

    $.trwa = 0;

    $.fn.karuzela = function(parametr) {
    
        var opcje = {
            src: 'mini/',
            dest: 'popup/',
            pliki: [],
            speed: 400
        };
        if (parametr) {
            $.extend(opcje, parametr);
        }
        if (opcje.pliki.length > 0) {
            var el = this.first()
                .append('<div id="element"></div><div><a href="#">&lt;&lt;</a><a href="#">&gt;&gt;</a></div>')
                .mousewheel(function(event, delta) {
                        if (delta > 0)
                            $(this).find('a').first().click();
                        else if (delta < 0)
                            $(this).find('a').last().click();

                });

            $.each(opcje.pliki, function(index, value){
                $('<img src="' + opcje.src + opcje.pliki[index] + '" alt="" />')
                    .css('left', 60 + index * 140 + 'px')
                    .mouseover(function(){
                        var maly = $(this).attr('src');
                        var duzy = maly.replace(opcje.src, opcje.dest);
                        $('#popup img').attr('src', duzy);
                        $('#popup').show();
                    }).mouseout(function(){
                        $('#popup').hide();
                    }).mousemove(function(e){
                        $('#popup').css('left', e.pageX + 10);
                        $('#popup').css('top', e.pageY + 10);
                    }).appendTo(el.children(':first-child'));
            });

            var imgs = this.find('img');

            el.children(':eq(1)').children(':first-child').click(function(){
                if (
                    !$.trwa &&
                    (imgs.last().css('left').replace('px', '') > 100)
                ) {
                    $.trwa = 1;
                    imgs.animate({
                        'left' : "-=140"
                    }, opcje.speed, function(){
                      $.trwa = 0;
                    });
                }
                return false;
            });

            el.children(':eq(1)').children(':last-child').click(function(){
                if (
                    !$.trwa &&
                    (imgs.first().css('left').replace('px', '') < 60)
                ) {
                    $.trwa = 1;
                    imgs.animate({
                        'left' : "+=140"
                    }, opcje.speed, function(){
                      $.trwa = 0;
                    });
                }
                return false;
            });

            $('<div id="popup"><img src="null.jpg" alt="" /></div>').prependTo('body');

        }
        return this;
    };
    
})(jQuery);