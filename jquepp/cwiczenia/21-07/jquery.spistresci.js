(function($) {

    $.fn.spistresci = function() {
        this.first().prepend('<ul/>');
        $('h1').attr('id', function(index, attr){
            $('#spistresci ul').append(
                '<li><a href="#R' +
                index +'">' +
                $(this).text() +
                '</a></li>'
            );
            return 'R' + index;
        });
        return this;
    };
    
})(jQuery);