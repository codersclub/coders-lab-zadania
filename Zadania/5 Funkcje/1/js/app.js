function sum10() {
    var result, i, max;

    result = 0;

    max = 11;
    for (i = 1; i < max; i += 1) {
        result += i;
    }

    return result;
}

console.log(sum10());