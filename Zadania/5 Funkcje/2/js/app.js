function sum(count) {
    var result, i, max;

    result = 0;

    max = count + 1;
    for (i = 1; i < max; i += 1) {
        result += i;
    }

    return result;
}

console.log(sum(10));