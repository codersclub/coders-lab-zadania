function sum(count, from) {
    var result, i, max;

    result = 0;

    if (typeof from === 'number') {
        i = from;
    } else {
        from = i = 1;
    }

    max = count + from;
    for (; i < max; i += 1) {
        result += i;
    }

    return result;
}

console.log(sum(2, 10));