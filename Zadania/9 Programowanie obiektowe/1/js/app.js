var person = {
    firstName: 'Artur',
    lastName: 'Delura',
    age: 24,
    sex: 'male',
    getTitle: function () {
        return (this.sex === 'male' ? 'Pan' : 'Pani') + ' ' + this.firstName + ' ' + this.lastName;
    }
};

console.log(person.getTitle());