function createPerson(firstName, lastName, age, sex) {
    return {
        firstName: firstName,
        lastName: lastName,
        age: age,
        sex: sex,
        getTitle: function () {
            return (this.sex === 'male' ? 'Pan' : 'Pani') + ' ' + this.firstName + ' ' + this.lastName;
        },
        createParagraph: function () {
            var p = document.createElement('p');
            p.innerText = this.getTitle();

            return p;
        }
    };
}

window.addEventListener('load', function () {
    var container = document.querySelector('.exercise-content'),
        jan = createPerson('Jan', 'Kowalski', 18, 'male');

    container.appendChild(jan.createParagraph());
});