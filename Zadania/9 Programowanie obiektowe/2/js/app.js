var person = {
    firstName: 'Artur',
    lastName: 'Delura',
    age: 24,
    sex: 'male',
    getTitle: function () {
        return (this.sex == 'male' ? 'Pan' : 'Pani') + ' ' + this.firstName + ' ' + this.lastName;
    },
    createParagraph: function () {
        var p = document.createElement('p');
        p.innerText = this.getTitle();

        return p;
    }
};

window.addEventListener('load', function () {
    var container = document.querySelector('.exercise-content');

    container.appendChild(person.createParagraph());
});