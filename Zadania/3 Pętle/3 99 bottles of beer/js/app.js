window.addEventListener('load', function() {
   var exerciseContent = document.getElementsByClassName('exercise-content')[0],
       lyricsLines = generateLyrics(),
       lyrics = '';

    var max = lyricsLines.length;
    for (var i = 0; i < max; i += 1) {
        lyrics += ('<p>' + lyricsLines[i] + '</p>');
    }

    exerciseContent.innerHTML = lyrics;
});