function generateLyrics(totalBottles) {
    var lyrics = [];

    totalBottles = totalBottles || 99;

    for (var bottles = totalBottles; bottles >= 0; bottles--) {
        if (bottles > 0) {
            lyrics.push(bottles + ' ' + plural('bottle', bottles) + '  of beer on the wall, ' + bottles + ' ' + plural('bottle', bottles) + ' of beer.');
            lyrics.push('Take one down and pass it around, ' + bottlesOnTheWall(bottles - 1));
        } else  {
            lyrics.push('No more bottles of beer on the wall, no more bottles of beer. ');
            lyrics.push('Go to the store and buy some more, ' + totalBottles + ' bottles of beer on the wall.');
        }
    }

    function bottlesOnTheWall(bottles) {
        if (bottles > 0) {
            return bottles + ' ' + plural('bottle', bottles) + ' of beer on the wall.';
        } else {
            return 'no more bottles of beer on the wall.';
        }
    }

    function plural(word, number) {
        return word + (number > 1 ? 's' : '');
    }

    return lyrics;
}