var ws = require('websocket.io'),
    server = ws.listen(3000),
    connections = [],
    messages = [];

console.log('Gniazdo nasłuchuje na porcie 3000.');

server.on('connection', function (socket) {
    console.log('Nowe połączenie.');
    // dodajemy połączenie do tablicy połączeń
    connections.push(socket);

    // przy otrzymaniu wiadomości z gniazda
    socket.on('message', function (message) {
        var data;
        console.log(message);
        // sprawdzamy poprawność dancyh
        try {
            data = JSON.parse(message);
        } catch(e) {
            console.log('Niepoprawne dane.');
            return;
        }

        // jeżeli użytkownik wysłał name, to ustawiamy to jako właściwość połączenia
        if (data.name) {
            console.log('Użytkownik ustawił swój nickname: ', data.name);
            socket.name = data.name;
        }

        // jeżeli użytkownik wysłał wiadomość
        if (data.message) {
            console.log('Wiadomość otrzymana od ' + socket.name + ': "' + data.message + '".');

            // to wysyłamy ją do wszystkich połączeń
            connections.forEach(function (connection) {
                connection.send(JSON.stringify({
                    message: data.message,
                    from: socket.name
                }));
            });
        }
    });

    // przy zakończonym połączeniu
    socket.on('close', function () {
        var socketIndex = connections.indexOf(socket);

        console.log('Połączenie zakończone');
        connections.splice(socketIndex, 1); // usuwamy je z tablicy
    });
});