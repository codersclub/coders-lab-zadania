window.addEventListener('load', function() {
    var wsUri = 'ws://127.0.0.1:3000',
        webSocket = new WebSocket(wsUri),
        msgContainer = document.querySelector('.chat ul'),
        nick;

    webSocket.onopen = function() {
        nick = prompt('Podaj swój nickname');

        var data = { name: nick };

        webSocket.send(JSON.stringify(data));
        form.message.focus();
    };

    webSocket.onmessage = function(e) {
        var data = JSON.parse(e.data);
        var me = (nick == data.from ? 'me' : '')

        var liElem = createElement([
            '<li class="', me , '" >',
                '<strong>', data.from, ': </strong>',
                '<span>', format(data.message), '</span>',
            '</li>'
        ].join(''));

        if (nick == data.from) liElem.classList.add('me');

        msgContainer.appendChild(liElem);

        form.message.value = '';
        msgContainer.scrollTop = msgContainer.scrollHeight;
    };

    function format(string) {
        return string.split('\n').join('</br>').split(' ').join('&nbsp;');
    }

    function createElement(string) {
        var element = document.createElement('p'),
            created;

        element.innerHTML = string;
        created = element.firstChild;
        element.removeChild(created);
        return created;
    }

    var form = document.querySelector('form');
    form.addEventListener('submit', function(e) {
        var data = { message: form.message.value };
        e.preventDefault();
        webSocket.send(JSON.stringify(data));
    });
});