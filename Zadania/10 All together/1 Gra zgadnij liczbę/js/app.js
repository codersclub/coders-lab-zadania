window.addEventListener('load', function () {
    var elem = document.getElementById('main');
    var result = document.getElementById('result');
    var form = document.getElementsByTagName('form')[0];
    var model = {
        checks: 0,
        secretNumber: generateSecret()
    };

    form.addEventListener('submit', function (e) {
        var number = Number(form.liczba.value);

        e.preventDefault();

        checkNumber(number);
        form.liczba.value = '';
    });

    function removeForm() {
        form.parentNode.removeChild(form);
    }

    function logMessage(message) {
        var p = document.createElement('p');

        p.innerHTML = message;
        result.appendChild(p);
    }

    function generateSecret() {
        return Math.round(Math.random() * 100);
    }

    function checkNumber(number) {
        var message;

        model.checks++;

        if (model.secretNumber > number) {
            message = 'Secret number is <b>greater</b> than ' + number;
        } else if (model.secretNumber < number) {
            message = 'Secret number is <b>lower</b> than ' + number;
        } else {
            message = '<b>Contgratulations!</b> You found the number which is <b>' + number + '</b>. You used <b>' + model.checks + '</b>';
            removeForm();
        }

        logMessage(message);
    }
});