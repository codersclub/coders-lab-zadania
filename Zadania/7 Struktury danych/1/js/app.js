var kursanci = [
    {firstName: 'Artur', lastName: 'Delura'},
    {firstName: 'Jan', lastName: 'Kowalski'}
];

var max = kursanci.length;

var content = document.getElementsByClassName('exercise-content')[0];
for (var i = 0; i < max; i += 1) {
    content.innerHTML += ('<p>' + kursanci[i].firstName + ' ' + kursanci[i].lastName + '</p>');
}