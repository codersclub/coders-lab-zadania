function fibonacci(number) {
    if (number === 0) {
        return 0;
    } else if (number === 1) {
        return 1;
    } else {
        return fibonacci(number - 1) + fibonacci(number - 2);
    }
}

function fibonacci(number) {
    var curr = 1, result = 0, prev;

    if (number === 0 || number === 1) return number;

    for (var i = 0; i < number; i += 1) {
        prev = result;
        result = prev + curr;
        curr = prev;
    }

    return result;
}

fibonacci(5);