window.addEventListener('load', function () {
    var form = document.getElementsByTagName('form')[0],
        avgSpeedContainer = document.querySelector('.avg-speed > strong'),
        paceContainer = document.querySelector('.pace > strong');

    form.addEventListener('submit', function (e) {
        e.preventDefault();
        var distance = parseDistance(form.distance.value),
            time = parseTime(form.time.value);

        avgSpeedContainer.innerText = calculateAverageSpeed(distance, time);
        paceContainer.innerText = formatPace(calculatePace(distance, time));
    });

    function parseDistance(distance) {
        var kilometers = distance.split('.')[0],
            meters = distance.split('.')[1],
            total = Number(kilometers * 1000) + Number(meters);

        return total;
    }

    function parseTime(time) {
        var hours = time.split(':')[0],
            minutes = time.split(':')[1],
            seconds = time.split(':')[2],
            total = Number(3600 * hours) + Number(60 * minutes) + Number(seconds);

        return total;
    }

    function calculateAverageSpeed(distance, time) {
        return distance / time * (3600 / 1000);
    }

    function calculatePace(distance, time) {
        return time / (distance / 1000) / 60;
    }

    function formatPace(pace) {
        var minutes = Math.floor(pace),
            seconds = pace - minutes;

        seconds = Math.floor(seconds * 60);

        return minutes + ':' + (seconds < 10 ? '0' + seconds : seconds);
    }
});