window.addEventListener('load', function() {
    var excerciseContent = document.getElementsByClassName('exercise-content')[0],
        lyrics = generateLyrics();

    var intervalId = setInterval(function() {
        var currentLine = lyrics.shift(),
            lineParagraph = document.createElement('p');

        lineParagraph.innerText = currentLine;

        excerciseContent.innerHTML = '';
        excerciseContent.appendChild(lineParagraph);

        if (lyrics.length == 0) {
            clearInterval(intervalId);
        }
    }, 3000);
});