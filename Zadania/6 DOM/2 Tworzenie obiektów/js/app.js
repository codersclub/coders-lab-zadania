window.addEventListener('load', function() {
    var logo = document.createElement('img'), // tworzymy element logo
        exerciseContent = document.getElementsByClassName('exercise-content')[0]; // pobieramy z drzewa DOM kontener na obrazek

    logo.src = './img/logo.png'; // ustawiamy mu atrybut src
    exerciseContent.appendChild(logo); // dodajemy obrazek do kontenera
});