window.addEventListener('load', function () {
    // główna lista miniaturek obrazków
    var list = document.getElementsByTagName('ul')[0];

    // kontener na duże obrazki
    var imageContainer = document.getElementsByClassName('image-container')[0];

    // duży obrazek
    var image = imageContainer.getElementsByTagName('img')[0];

    list.addEventListener('click', function(e) {
        var clicked = e.srcElement;

        if (!(clicked instanceof HTMLImageElement)) {
            return;
        }

        image.src = clicked.src.replace('thumb', 'original');
    });
});